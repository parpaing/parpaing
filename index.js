const Manager = require('./src/Manager');
const Mod = require('./src/Mod');

module.exports = { Manager, Mod };
