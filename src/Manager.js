const path = require('path');

const winston = require('winston');

const ConfigSystem = require('./ConfigSystem');
const ConfigSchema = require('./ConfigSchema');

class Manager {
    constructor() {
        this.root = path.dirname(require.main.filename);

        this.configSys = new ConfigSystem(this.root);
        this.configSys.validate(ConfigSchema);
        this.config = this.configSys.configObj;

        this.logger = winston.createLogger({
            level: this.config.logger.level,
            format: winston.format.combine(winston.format.splat(), winston.format.simple()),
            transports: [
                new winston.transports.Console()
            ]
        });

        this.mods = [];
    }

    async setup(block, dependencies, from) {
        const mod = new block.Mod(this);

        if (typeof from !== 'undefined') {
            mod.from(from);
        }
        if (typeof dependencies !== 'undefined') {
            mod.with(dependencies);
        }
        mod.validConfig(block.ConfigSchema);

        this.logger.verbose(`[${mod.constructor.name}] Setup started.`);
        await mod.setup();
        this.logger.verbose(`[${mod.constructor.name}] Setup finished.`);

        this.mods.push(mod);
        return mod;
    }

    async init() {
        this.mods.forEach((module) => module.init());
    }
}

module.exports = Manager;
