const Joi = require('@hapi/joi');

const ConfigSchema = Joi.object().keys({
    node_env: Joi.string().valid('local', 'development', 'staging', 'production').required(),
    logger: Joi.object().keys({
        level: Joi.string().valid('error', 'warn', 'info', 'verbose', 'debug', 'silly').default('info')
    })
}).unknown().required();

module.exports = ConfigSchema;
