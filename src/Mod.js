const path = require('path');

const ow = require('ow');

const ConfigSystem = require('./ConfigSystem');

class Mod {
    constructor(Manager) {
        this.Manager = Manager;
        this.logger = Manager.logger;

        this.config = {};
        this.deps = {};
        this.mod = [];

        this.root = path.join(this.Manager.root, this.constructor.name);
    }

    validConfig(configSchema) {
        this.configSys = new ConfigSystem(this.root, this);
        this.configSys.validate(configSchema);
        this.config = this.configSys.configObj;
    }

    setup() {
    }

    init() {
    }

    from(root) {
        ow(root, ow.string);

        this.root = path.resolve(this.Manager.root, root);
    }

    with(dep) {
        ow(dep, ow.object);

        Object.assign(this.deps, dep);
    }

    setMod(key, value) {
        ow(key, ow.string);

        this.mod[key] = value;
    }

    getMod(key) {
        ow(key, ow.string);

        return this.mod[key];
    }
}

module.exports = Mod;
