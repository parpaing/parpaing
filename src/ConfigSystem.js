const fs = require('fs');
const path = require('path');

const Joi = require('@hapi/joi');
const JSONTemplates = require('json-templates');

class ConfigSystem {
    constructor(root, Mod) {
        this.root = root;
        this.Mod = Mod;

        this.configObj = {};

        this.readConfig();
    }

    readConfig() {
        const configPath = path.join(this.root, 'config.json');

        if (!fs.existsSync(configPath) || !fs.statSync(configPath).isFile()) return;

        try {
            const configJSON = JSON.parse(fs.readFileSync(configPath).toString());
            Object.assign(this.configObj, JSONTemplates(configJSON)(process.env));
        } catch (err) {
            if (typeof this.Mod !== 'undefined') {
                this.Mod.logger.silly(`[${this.Mod.constructor.name}] Config file ${configPath} is invalid: ${err}`);
            } else {
                throw new Error(`Config file ${configPath} is invalid: ${err}`);
            }
        }
    }

    validate(configSchema) {
        const schema = configSchema || Joi.any();
        const { error, value } = schema.validate(this.configObj);

        if (error) {
            if (typeof this.Mod !== 'undefined') {
                throw new Error(`[${this.Mod.constructor.name}] Config is invalid: ${error}`);
            } else {
                throw new Error(`Config is invalid: ${error}`);
            }
        }

        this.configObj = value;
        if (typeof this.Mod !== 'undefined') {
            this.Mod.logger.verbose(`[${this.Mod.constructor.name}] Config validated.`);
        }
    }
}

module.exports = ConfigSystem;
